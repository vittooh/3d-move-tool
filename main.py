from pyglet.gl import *
from pyglet.window import mouse

from pywavefront import visualization, Wavefront

window = pyglet.window.Window(width=600, height=720)

box1 = Wavefront('/home/vitor/Desenvolvimento/Move3dTool/model/box-V3F.obj')

rotation = 0.0
zoom = -10


# acho que isso é um decorator, não sei como funciona, mas parece que pra
# para a minha função ser chamada tem que anotar isso com @
@window.event
def on_resize(width, height):
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45., float(width) / height, 1., 100.)
    glMatrixMode(GL_MODELVIEW)
    return True


@window.event
def on_draw():
    window.clear()
    glLoadIdentity()
    glTranslated(0.0, 0.0, zoom)
    put_3d_model_screen(box1, 0.0, 0.0)


# No sistema de coordenadas tridimensional, um ponto P no espaço é determinado por um terno ordenado (x,y,z) onde x,
# y e z são definidos da seguinte forma: X: distância do ponto P ao plano yz y: distância do ponto P ao plano xz z:
# distância do ponto P ao plano xy
# por isso que mexo no inferno do z e ele cria o efeito de zoom!

def mouse_zoom_handler(zoom_event):
    global zoom
    zoom += zoom_event


@window.event
def on_mouse_scroll(x, y, scroll_x, scroll_y):
    # x  e y de onde o mouse esta == até onde entendi o y -1 e mouse retirando zoom e y = 1 mouse dando zoom
    mouse_zoom_handler(scroll_y)


@window.event
def on_mouse_motion(x, y, dx, dy):
    print("Mexendo em x:: {0} y :: {1}  distancia percorrida  x : {2} y : {3}".format(x,y,dx,dy))


def put_3d_model_screen(model, x, y):
    glLoadIdentity()
    glTranslated(x, y, zoom)
    glRotatef(rotation, 0.0, 1.0, 0.0)
    glRotatef(-25.0, 1.0, 0.0, 0.0)
    glRotatef(45.0, 0.0, 0.0, 1.0)

    visualization.draw(model)


def update(dt):
    global rotation
    rotation += 90.0 * dt

    if rotation > 720.0:
        rotation = 0.0


pyglet.clock.schedule(update)
pyglet.app.run()
